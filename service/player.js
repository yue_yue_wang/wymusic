import { wyRequest} from "./index"
export function getSongsDetail(ids){
  return wyRequest.get("/song/detail",{ids})
}

export function getSongLyric(id){
  return wyRequest.get('/lyric',{id})
}