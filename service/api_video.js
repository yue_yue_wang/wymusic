import { wyRequest } from "./index.js"

export function getTopMV(offset,limit=10){
  return wyRequest.get("/top/MV",{offset,limit})
}   

// 获取播放地址
export function getMVUrl(id){
  return wyRequest.get('/mv/url',{id})
}

// 获取mv详情
export function getMVDetail(mvid){
  return wyRequest.get('/mv/detail',{mvid})
}

// 获取mv相关视频
export function getRelatedVideo(id){
  return wyRequest.get('/related/allvideo',{id})
}