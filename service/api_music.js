import {wyRequest} from "./index"

export function getBanner(){
  return wyRequest.get('/banner',{type:2})
}

export function getRanking(idx){
  return wyRequest.get('/top/list',{idx})
}

export function getSongMenu(cat="全部",limit=6,offset=0){
  return wyRequest.get("/top/playlist",{
    cat,limit,offset
  })
}

export function getSongsDetail(id){
  return wyRequest.get('/playlist/detail/dynamic',{id})
}

export function getearchHot(){
  return wyRequest.get("/search/hot")
}

export function getSuggest(keywords){
  return wyRequest.get('/search/suggest',{keywords,type:"mobile"})
}

export function getSearchRes(searchValue){
  return wyRequest.get("/search",{keywords:searchValue})
}
