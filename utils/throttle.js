export default function throttle(func, delay,options) {
  // 使用定时器和时间戳完成的第三版（第一次会触发，最后一次会触发）
  let context,args,timeout;
  // 之前的时间戳
  let oldTime = 0
  if (!options) options={}
  let later = function () {
      oldTime = new Date().valueOf()
      timeout = null
      func.apply(context,args)
  }
  return function () {
      context = this
      args = arguments

      // 获取当前时间戳
      let now = new Date().valueOf()

      // 判断第一次是否执行
      if (options.leading === false){
          // 不执行
          oldTime = now
      }

      // 第一次会触发
      if (now-oldTime > delay){
          if (timeout){
              // 清空定时器
              clearTimeout(timeout)
              timeout = null
          }
          // 立即执行
          func.apply(context,args)
          // 每隔delay秒执行一次
          oldTime = now
      }else if (!timeout && options.trailing!==false){
          timeout = setTimeout(later,delay)
      }
  }
}