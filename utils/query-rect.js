export default function(selector){
// 获取组件的宽高  
  return new Promise((resolve) => {
    var query = wx.createSelectorQuery()
    query.select(selector).boundingClientRect()
    query.exec(res => {
      resolve(res)
      })
    })
}