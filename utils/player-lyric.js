//[00:58.65]
const timePattern = /\[(\d{2}):(\d{2})\.(\d{2,3})\]/
export function parseLyric(lyricString){
  const lyricStrings = lyricString.split('\n')
  const list = []
  lyricStrings.forEach(item => {
    const timeRes = timePattern.exec(item)
    let time = ''
    if(timeRes){
      const minute = timeRes[1] * 60 * 1000
      const second = timeRes[2] * 1000
      const millsecond = timeRes[3].length === 2 ?timeRes[3] * 10 :timeRes[3]
      time = minute + second + millsecond
      const text = item.replace(timeRes[0],'')
      list.push({time,text})
    }
  })
  return list
}