import {
  getSongsDetail,
  getSongLyric
} from "../service/player.js"
import {HYEventStore} from "hy-event-store"
import {parseLyric} from "../utils/player-lyric"


const audioContext = wx.createInnerAudioContext()
const playerStore  = new HYEventStore({
  state:{
    currentSong: {},
    duration: 0,
    lyrics:[],
  },
  mutations:{
  },
  actions:{
    playMusicWithIdAction(ctx,payload){
      console.log('===========');
      getSongsDetail(payload.id).then(res => {
        console.log(res,'----------------');
        if(!res.songs) return  
        ctx.currentSong = res.songs[0]
        ctx.duration = res.songs[0].dt
      })
      getSongLyric(payload.id).then(res =>  {
        const lyricString = parseLyric(res.lrc.lyric)
        ctx.lyrics = lyricString
      })
    }
  }
})
export {
  audioContext,
  playerStore
}