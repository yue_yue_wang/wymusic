import {
  HYEventStore
} from "hy-event-store"
import {
  getRanking
} from "../service/api_music"

export const rankingStore = new HYEventStore({
  state: {
    newRanking:{},
    hotRanking: {},
    originRanking:{},
    upRanking:{}
  },
  actions: {
    getRankingData(ctx) {
      // 0新歌榜 1热歌榜 2原创榜 3飙升榜
      for (let i = 0; i < 4; i++) {
        getRanking(i).then(res => {
          switch (i) {
            case 0:
              ctx.newRanking = res.playlist
              break
            case 1:
              ctx.hotRanking = res.playlist
              break
            case 2:
              ctx.originRanking = res.playlist
              break
            case 3:
              ctx.upRanking = res.playlist
              break
          }
        })
      }

    }
  }
})