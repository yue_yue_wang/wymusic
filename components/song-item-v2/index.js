// components/song-item-v2/index.js
import {playerStore} from "../../store/index.js"

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    item:{
      type:Object,
      value:{}
    },
    index:{
      type:[Number,String],
      value:0
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    songClick: function (event) {
      console.log(11111111111);
      const id = event.currentTarget.dataset.id
      wx.navigateTo({
        url: '/pages/music-player/index?id=' + id,
      })
// 请求详情页数据
playerStore.dispatch("playMusicWithIdAction",{id})
    }
  }
})
