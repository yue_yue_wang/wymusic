
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    item: {
      type: Object,
      value: {}
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    songClick: function (event) {
      console.log(11111111111,playerStore);
      const id = event.currentTarget.dataset.id
      // 页面跳转
      wx.navigateTo({
        url: '/pages/music-player/index?id=' + id,
      })
      
    }
  }
})