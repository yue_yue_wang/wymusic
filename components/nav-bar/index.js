// components/nav-bar/index.js
import {NavBarHeight} from "../../constants/device-const"
Component({
  /**
   * 组件的属性列表
   */
  options:{
    multipleSlots:true //使用多个插槽
  },
  properties: {
    title:{
      type:String,
      value:'默认标题'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    statusBarHeight:getApp().globalData.statusBarHeight,
    NavBarHeight:0
  },
  // 生命周期函数
  lifetimes:{
    ready:function(){
      // const info = wx.getSystemInfoSync()
      this.setData({NavBarHeight:NavBarHeight})
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {

  }
})
