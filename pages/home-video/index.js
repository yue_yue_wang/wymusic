
import { getTopMV } from "../../service/api_video.js"
Page({   

  /**  
   * 页面的初始数据nn
   */
  data: {
    topMVs:[],
    isMore:true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getTopMData(0)
  },

  // 网络请求方法
  getTopMData(offset){
    if(!this.data.isMore) return 
    // 展示加载动画
    if(offset === 0){
      wx.showNavigationBarLoading()
    }
    getTopMV(offset).then(res => { 
      if(offset === 0){
        this.setData({topMVs:res.data})
      }else{
        this.setData({topMVs:this.data.topMVs.concat(res.data)})
        this.setData({isMore:res.hasMore})
        wx.hideNavigationBarLoading()
        if(offset === 0){
          wx.stopPullDownRefresh()
        }
      }
    })
  },
  // 事件处理方法
  handelVideoItemClick(event){
    const id = event.currentTarget.dataset.item.id
    wx.navigateTo({
      url: '/pages/detail-video/index?id=' + id,
    })

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },           

  /**
   * 生命周期函数--监听 页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {
    this.getTopMData(0)
  }, 

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {  
    this.getTopMData(this.data.topMVs.length)
  },
 
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})