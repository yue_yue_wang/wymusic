// pages/home/index.js
import {getBanner,getSongMenu} from "../../service/api_music"
import QueryRect from "../../utils/query-rect"
import throttle from "../../utils/throttle"
import {rankingStore} from "../../store/ranking-store"
// 节流 
// var throttleQuery = throttle(QueryRect)
Page({

  /**
   * 页面的初始数据
   */
  data: {
    banners:[],
    swiperHeight:125,
    recommendSongs:[],
    hotSongMenu:[],
    recommendSongMenu:[],
    rankings:{0:{},2:{},3:{}}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // 轮播图数据
    this.getPageData()
    rankingStore.dispatch('getRankingData')
    rankingStore.onState('hotRanking',(res) => {
      if(res?.tracks){
        const recommendSongs = res?.tracks?.slice(0,6)
        this.setData({recommendSongs})
      }
     
    })  
      rankingStore.onState('newRanking',this.getLsit(0))
    rankingStore.onState('originRanking',this.getLsit(2))
    rankingStore.onState('upRanking',this.getLsit(3))
    
  }, 
  getPageData:function(){
    getBanner().then(res => {
      this.setData({banners:res.banners})
    })
    getSongMenu().then(res => {
      this.setData({hotSongMenu:res.playlists})
    })
    getSongMenu("古风").then(res => {
      this.setData({recommendSongMenu:res.playlists})
    })
  },
  handleSearch:function(){
    wx.navigateTo({
      url: '/pages/detail-search/index',
    })
  },
  handleImgLoaded:function(){
    // throttleQuery('.swiper-image').then(res => {
    //   this.setData({swiperHeight:res[0].height})
    // })
    QueryRect('.swiper-image').then(res => {
      this.setData({swiperHeight:res[0].height})
    })
  },
  getLsit:function(idx){
    return (res) => {
      if(Object.keys(res).length){
        var name = res.name 
        var coverImgUrl = res.coverImgUrl
        var songList = res.tracks?.slice(0,3)
        var playCount = res.playCount
        var rankObj = {name,coverImgUrl,songList,playCount}
        var newRnakings = {...this.data.rankings,[idx]:rankObj}
        this.setData({rankings:newRnakings})
      }
    }
    
  },
  handleMoreClick:function(){
    this.navigateToDetailPage('hotRanking')
  },
  handelRankingItemClick:function(event){
    const idx = event.currentTarget.dataset.idx
    const rankingMap = {0:'newRanking',2:"originRanking",3:"upRanking"} 
    const rankingName = rankingMap[idx]
    this.navigateToDetailPage(rankingName)
  },
  navigateToDetailPage:function(name){
    console.log(name);
    wx.navigateTo({
      url: `/pages/detail-songs/index?ranking=${name}&type=ranking`,
    })
  }
})