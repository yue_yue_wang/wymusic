// pages/detail-video/index.js
import {getMVUrl,getMVDetail,getRelatedVideo} from "../../service/api_video"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    mvURLInfo:{},
    relatedVideos:[],
    mvDetail:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getPageData(options.id)
  },
  getPageData(id){
    // 请求播放地址
    getMVUrl(id).then(res => {
      // setData:在设置data数据是同步的；通过最新的数据对wxml进行渲染的过程是异步
      this.setData({mvURLInfo:res.data})
    })
    // 请求mv详情
    getMVDetail(id).then(res => {
      this.setData({mvDetail:res.data})

    })
    // 请求mv相关视频  
    getRelatedVideo(id).then(res => {
      this.setData({relatedVideos:res.data})
      console.log(res);
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})