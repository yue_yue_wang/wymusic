// pages/detail-songs/index.js
import {rankingStore} from "../../store/ranking-store"
import {getSongsDetail} from "../../service/api_music"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    ranking:[],
    title:"",
    type:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.setData({type:options.type})
    if(options.type === "menu"){
      getSongsDetail(options.id).then(res => {
       this.setData({ranking:res.playlist})
      }) 
    }else{
      rankingStore.onState(options.ranking,this.getRankingData)
    }
  },
  getRankingData:function(res) {
    this.setData({ranking:res})
    this.setData({title:res.name})
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    if(this.data.ranking){
      rankingStore.offState(this.data.ranking,this.getRankingData)
    }
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})