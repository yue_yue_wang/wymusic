// pages/music-player/index.js

import {
  NavBarHeight
} from "../../constants/device-const"
import {
  audioContext,playerStore
} from "../../store/index.js"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: '',
    currentSong: {},
    duration: 0, 
    lyrics:[],
    currentTime: 0,
    currentLyricIndex:0,
    currentLyric:'',

    countHeight: 0,
    currentIndex: 0,
    sliderValue: 0,
    isSliderChange: false,
    lyricScrollTop:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const id = options.id
    this.setData({
      id
    })
    const screenHeight = getApp().globalData.screenHeight
    const statusBarHeight = getApp().globalData.statusBarHeight
    const countHeight = screenHeight - statusBarHeight - NavBarHeight
    this.setData({
      countHeight
    })
    this.setupPlayerStor(id)
    // 播放歌曲
    audioContext.stop() //先停止上一个歌曲
    audioContext.src = `https://music.163.com/song/media/outer/url?id=${id}.mp3`
    audioContext.autoplay = true //自动播放

    // 准备好歌曲流，可以播放
    audioContext.onCanplay(() => {
      audioContext.play()
    })
    // 监听时间改变
    audioContext.onTimeUpdate(() => {
      const currentTime = audioContext.currentTime * 1000
    // 根据当前时间修改currentTime、sliderValue
      if (!this.data.isSliderChange) {
        this.setData({
          currentTime
        })
        const sliderValue = currentTime / this.data.duration * 100
        this.setData({
          sliderValue
        })
      }
      const sliderValue = currentTime / this.data.duration * 100
      this.setData({
        sliderValue,
        isSliderChange: false
      })

      // 根据当前时间修改歌词
      for(let i = 0; i < this.data.lyrics.length; i++){
        const lyricInfo = this.data.lyrics[i]
        if(currentTime  < lyricInfo.time){
          const currentIndex = i - 1
          const currentLyric = this.data.lyrics[i-1]?.text
          if(this.data.currentLyricIndex !== currentIndex){
            this.setData({currentLyric})
            this.setData({currentLyricIndex:currentIndex})
            this.setData({lyricScrollTop:currentIndex * 35})
            break
          }
        }
      }
    })
  },
  // 监听store数据
  setupPlayerStor:function(id){
    playerStore.onStates(['currentSong','duration','lyrics'],(res) => {
      console.log(res);
      if(res.currentSong) this.setData({currentSong:res.currentSong})
      if(res.duration) this.setData({duration:res.duration})
      if(res.lyrics) this.setData({lyrics:res.lyrics})
      console.log(res);
    })
  },
  // 事件处理
  handleSwiperChange: function (event) {
    const current = event.detail.current
    this.setData({
      currentIndex: current
    })
  },
  handleSliderChange: function (event) {
    // 获取slider变化的值
    const value = event.detail.value
    const currentTime = (this.data.duration * value / 100)

    // 播放currentTime的音乐
    audioContext.pause() //音乐先暂停一下
    audioContext.seek(currentTime / 1000)

    this.setData({
      sliderValue: value
    })
  },
  handleSliderChangeing: function (event) {
    const value = event.detail.value
    const currentTime = this.data.duration * value / 100
    this.setData({
      isSliderChange: true,
      currentTime
    })
  }


})