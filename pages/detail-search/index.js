import {getearchHot,getSuggest,getSearchRes} from "../../service/api_music"
// pages/detail-search/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hotKeywords:[],
    suggestList:[],
    suggestListNodes:[],
    keyValue:"",
    songs:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getPageData()
  },
  getPageData:function(){
    getearchHot().then(res => {
      console.log(res);
      this.setData({hotKeywords:res.result.hots})
    })
  },
    handleSearch:function(event){
      const suggestVlue = event.detail
      this.setData({keyValue:suggestVlue})
      if(!suggestVlue.length){
        this.setData({suggestList:[]})
        this.setData({songs:[]})
        return 
      }
      // 防抖
     setTimeout(() => {
      getSuggest(suggestVlue).then(res => {
        this.setData({suggestList:res.result.allMatch})
        const keywords = res.result.allMatch.map((item) => item.keyword)
       const suggestListNodes = []
        for(const keyword of keywords){
         const nodes = []
        if(keyword.toUpperCase().startsWith(suggestVlue.toUpperCase())){
          const key1 = keyword.slice(0,suggestVlue.length)
          const key2 = keyword.slice(suggestVlue.length)
          const node1 = {
            name:'span',
            attrs:{
              style:'color:red'
            },
            children:[{type:'text',text:key1}]
          }
          nodes.push(node1)
          const node2 = {
            name:'span',
            children:[{type:'text',text:key2}]
          }
          nodes.push(node2)
        }
        suggestListNodes.push(nodes)
       }
       this.setData({suggestListNodes})

      })
     },500)
    },
    handleSearchAction:function(event){
      const searchValue = this.data.keyValue
      getSearchRes(searchValue).then(res => {
        console.log(res,11111);
        this.setData({songs:res.result.songs})
      })
    },
    // 热门搜索
    handleTagItem:function(event){
      const keyword = event.currentTarget.dataset.item
      this.setData({keyValue:keyword})
      getSearchRes(keyword).then(res => {
        console.log(res);
        this.setData({songs:res.result.songs})
      })
    }

  
})